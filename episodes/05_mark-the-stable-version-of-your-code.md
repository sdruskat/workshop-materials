<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
SPDX-FileCopyrightText: 2020 Katrin Leinweber

SPDX-License-Identifier: CC-BY-4.0
-->

# Step 5: Mark the stable version of your code

## Why?

- Otherwise users do not know which version is considered stable.
- Otherwise users do not exactly know which version has been used to produce a specific result.

## Release Basics

- A *release* is a stable version of your software.
- The *release number* uniquely identifies the released software version.
  There are different release number schemes such as Semantic Versioning and Calendar Versioning.
- A *release tag* is used to mark the release content in your source code repository.
  It should be named in accordance to the release number.
- A *release package* provides the executable code including proper documentation to the user.
- The *changelog* documents all released versions of your software including the major changes of these versions.
  It is written form the user`s point of view.

## Minimal Release Checklist

Before your start, please make sure that you:

- Define which release number scheme you want to use.
- Define how release tags are named.

### 1. Prepare your code for release

- Make sure that your code fulfills its intended functionality and test it accordingly on the basis of package (release package) that you provide to your users.
- Define the release number for the release.
- Document the user-visible changes with the help of a changelog for this release.

### 2. Create a release tag

- Mark the release in the repository using a tag.
- This step makes sure that we find the content of the released version in the source code repository.

## Example Project

- [`astronaut-analysis/compare/5-prepare-for-release`](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/compare/4-add-license-infos...5-prepare-for-release)
  shows the result of this step:
  - Added a changelog

## Key Points

- Mark usable, stable versions of your software using release numbers and tags
- Document important changes with the help of a changelog

## Challenges

- [ ] Please find out more about the different release number schemes and select a scheme for your code.
- [ ] Please find out more about writing proper changelogs and create a suitable changelog for your code.
- [ ] Please find out more about release-specific support in GitLab and GitHub.

## Further Readings

- [Semantic Versioning](https://semver.org/)
- [Calendar Versioning](https://calver.org/)
- [Git Tags](https://git-scm.com/book/en/v2/Git-Basics-Tagging)
- [keep a changelog](https://keepachangelog.com/)
