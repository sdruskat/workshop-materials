<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Step 6: Make your code citable

## Why?

- Software is a research product, just like a paper or a monograph.
- Creating and maintaining research software is academic work, and should allow for academic credit and careers.
- Citing software is an important part of the provenance of research results and enables reproducibility.

### Digitalization of Research has made Software a central Asset of Research

- Creation and maintenance of research software requires knowledge and research.
  Research software work is therefore academic work and it should be treated on par with papers and books.
- In the current system, credit is provided for publication and (re-) use of academic work.
  Academic careers are (partly) built on such credit.
- The common way to award credit is citation of work that is used in or built upon one's own work.

### Software Citation enables Reproducibility of Research Results

- The provenance record of a research result should include the software that has been used to achieve the result.
- Correct software citation makes it possible to reproduce and evaluate the research result as well as to find and evaluate the research software itself.

## How to cite Software?

- Please make sure that you cite all software packages (including your own) in the reference list of your academic work.
- Please follow the guidelines outlined in [Research software citation for researchers](https://research-software.org/citation/researchers/).
  These guidelines provide hints where to look for relevant information and how to cope with incomplete citation metadata.

## How to make your Software citable?

- Please make sure that other persons can easily cite your software by:
  - Providing citation metadata,
  - Obtaining a persistent identifier (PID) for your software and
  - Providing a citation hint as part of your documentation.
- You can directly manage citation metadata as part of your source code repository by
  providing a file `codemeta.json` (machine-readable information about your software including citation metadata) **or**
  providing a file `CITATION.cff` (human/machine-readable citation metadata).
- Another option is to let digital repositories such as Zenodo manage your citation metadata.
- A PID is crucial to ensure long-term availability of your software as repository URLs may break easily over time:
  - Please make sure that you have a PID for every released version of your software.
  - Digital repositories allow to archive your software and provide a PID - usually a digital object identifier (DOI).
  - Software Heritage already archives many publicly available source code repositories and automatically assigns them a PID.
    You can also pro-actively save your code to Software Heritage for every release.
    In addition, the Software Heritage harvests metadata contained in the repository and allows you to find source code repositories based on this metadata. 
- In summary, there are currently two practical solutions:
  - Deposit your software in a digital repository which manages citation metadata and provides a DOI.
    Particularly, Zenodo is a good solution because it provides an integration with GitHub and supports versioned DOIs.
  - Publish your software on a public code hosting platform, add citation metadata to the repository and use the automatically generated Software Heritage PID for reference.
    Unfortunately, this approach currently misses some tool support such as citation generators.
- In addition, you may still consider writing a software paper.
  Particularly, if your organization only "awards" peer-reviewed publications.
  Specifically, we recommend to publish your software paper in the Journal of Open Source Software because of its developer-friendly approach.

### Authorship and Contributorship in Software

- There are no universally accepted **guidelines** for software authorship
- Different **roles** than `programmers` may be the authors of a software.
  For example, testers, reviewers, technical writers, maintainers, release engineers, software architects, UX designers, etc., may all qualify for authorship.
- **Decisions** about authorship are project-specific, but must follow good scientific practice.
  Refer to the [ICMJE Uniform Requirements](http://www.icmje.org/recommendations/browse/roles-and-responsibilities/defining-the-role-of-authors-and-contributors.html) for papers, and translate the best practice to software, e.g.
  - There may be *no* honorary authorship.
  - The contribution to the software must be substantial.
  - Final approval of the outcome may be covered by a CLA.
  - Agreement to be accountable for - at least the own - changes may be factored into the decision.
- **Contributors** should be acknowledged, but are not software authors.
  Typical examples include issue reporters, typo fixers, evangelists promoting the software, and managers/PIs with no substantial contribution to the software itself.
  Consider using automation to keep track of contributors, for example, with the help of the [All Contributors bot](https://allcontributors.org)

## Example Project

- [`astronaut-analysis/compare/6-prepare-for-citation`](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/compare/5-prepare-for-release...6-prepare-for-citation)
  shows the result of this step:
  - Added a citation hint

## Key Points

- Cite all relevant software packages as good as possible in your academic work
- Provide relevant information and encourage citation

## Challenges

### During the Workshop

- [ ] Please learn more about Zenodo and its GitHub integration by following [the Open Science MOOC tutorial](https://github.com/OpenScienceMOOC/Module-5-Open-Research-Software-and-Open-Source/blob/master/content_development/MAIN.md#using-github-and-zenodo-).
- [ ] Please learn more about Software Heritage and follow their [Save and reference research software](https://www.softwareheritage.org/save-and-reference-research-software/) tutorial.
- [ ] Please experiment with citation metadata files for your code by following [the examples on the Research Software Citation website](https://research-software.org/citation/developers/).
- [ ] Please find out more about suitable software journals for your domain.

### After the Workshop

- [ ] Please find out whether your organization provides recommendations with regard to software citation. 
- [ ] Please select a suitable approach for your code (Zenodo vs. Software Heritage vs. software journal) and make your code citable.
  Please make sure to add a citation hint to your documentation.

## Further Readings

### General

- [Research Software Citation](https://research-software.org/citation/)
- Chue Hong, Neil. (2019, May). How to cite software: current best practice. Zenodo. https://doi.org/10.5281/zenodo.2842910
- [CodeMeta Project](https://codemeta.github.io/), [CodeMeta Generator](https://codemeta.github.io/codemeta-generator/),
  [CodeMeta Generator on GitHub](https://github.com/codemeta/codemeta-generator/)
- [Citation File Format](https://citation-file-format.github.io)
- [Zenodo](https://zenodo.org/)
- [Software Heritage](https://www.softwareheritage.org/),
  [Software Heritage Meta Data Support](https://www.softwareheritage.org/2019/05/28/mining-software-metadata-for-80-m-projects-and-even-more/)

### Software Journals

- [Journal of Open Source Software](https://joss.theoj.org/)
- [Journal of Open Research Software](https://openresearchsoftware.metajnl.com/)
- [Overview of Software Journals](https://www.software.ac.uk/which-journals-should-i-publish-my-software)
