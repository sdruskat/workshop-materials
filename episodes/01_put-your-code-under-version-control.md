<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
SPDX-FileCopyrightText: 2020 Katrin Leinweber

SPDX-License-Identifier: CC-BY-4.0
-->

# Step 1: Put your code under version control

## Why?

- We want to share it and need to improve some aspects.
  If something goes wrong, we want to be able to easily go back to a specific state.
- Using Git offers a straight forward way to share the code and to collaborate on it with others.

## Where should I store my code?

- As a very minimum, you should use a local Git repository.
  You can initialize it via `git init`.
  Then, you use `git add`, `git commit` and `.gitignore` to control what goes into the repository.
  Make small commits as they allow you to go back to a specific state easily.
- At the end of the day, you should backup your work.
  You could use a simple available backup solution but it is preferable to use a collaboration platform such as GitLab or GitHub.
  In addition to backing up your work, such platforms offer a straight path to collaboration.
- Whether you can use a public collaboration platform depends on your organizational policies and your case.
  However, it is at least worth to find out whether this is possible or whether a specific internal collaboration platform exists.

## What belongs into the repository?

- Generally, you **should put** everything into your Git repository that is required to create a usable version of your code and to produce the intended results.
  This includes the source code, documentation, build scripts, test cases, configuration files, input data and other things.
- This approach has its limits.
  Generally, we recommend to  avoid adding generated artifacts to your Git repository such as
  third-party libraries,
  very large binaries (preferred maximum Git repository size is about 1 Gigabyte),
  and files which should be published separately (e.g., datasets, databases).
  However, the specific decision depends on the concrete case.
- In case it is not useful to add a required artifact to the Git repository, please make sure to reference it in a reliable way.
- If your input data set is too large, please consider publishing the data set separately.
  [re3data.org](http://re3data.org/) provides an overview about public research data repositories.

## Example Project

- [`astronaut-analysis/compare/master...1-put-into-git`](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/compare/0-original-source...1-put-into-git) shows the result of this step:
  - We put the original code into a local Git repository and pushed it to the GitLab instance.
  - We removed the generated image files and added a `.gitignore` to ignore them in future.

## Key Points

- Version control helps you to prepare the code for sharing.
- Make sure to put all relevant artifacts into the repository.
- `.gitignore` helps you to specify things that you do not want to share.


## Challenges

### During the Workshop

- [ ] Please put your code into a Git repository.
  For participants without a personal code project, please practice this step using the
  [`astronaut-analysis` example](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/)
  example code ([download branch `0-original-source`](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/-/archive/0-original-source/astronaut-analysis-0-original-source.zip)).
- [ ] Sometimes it is not suitable to store, particularly, large input data in the Git repository directly.
  Git LFS is an alternative in such a case.
  Please learn more about it by following the links below.
- [ ] Please think about the artifacts in your repository.
  Is there any room for improvements?
  If possible, please implement the identified improvements and clean up your repository accordingly.
  - [ ] If you have not already done it, please create a suitable `.gitignore` file and add it to your repository.
  - [ ] You can remove unwanted files using `git filter-branch` or `BFG Repo-Cleaner` (see below).
    But be aware that these tools change your Git repository history!


### After the Workshop

- [ ] Please find out where you should normally store code in your organization.
- [ ] Please find out how the process in your organization works to share your code publicly.

## Further Readings

- [The Software Carpentry Git lessons](https://swcarpentry.github.io/git-novice/) offers a good starting point to learn more about Git.
- [gitignore.io](https://gitignore.io/) generates `.gitignore` files for different combinations of operating systems, development environments, and programming languages.
- [Git Large File Storage (LFS)](https://git-lfs.github.com/) allows to handle large files in connection with Git.
  [GitLab](https://docs.gitlab.com/ee/topics/git/lfs/) and [GitHub](https://help.github.com/en/github/managing-large-files/configuring-git-large-file-storage) and other collaboration platforms offer support for Git LFS.
- [git filter-branch](https://git-scm.com/docs/git-filter-branch) and [BFG Repo-Cleaner](https://rtyley.github.io/bfg-repo-cleaner/) allow you to remove unwanted files from your Git repository history.
